<?php

namespace App;

use App\Database as DB;

use PDO;

//include("Database.php");

class Booktitle extends DB
{

    public $conn;

    public $id = "";

    public $book_name = "";

    public $author_name = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function index()
    {


        $conn = $this->DBH;
        $STH = $conn->prepare("select * from book_title");
        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $STH->fetch()) {

            echo "<br/>" . $row['id'] . "<br/>";

            echo $row['book_name'] . "<br/>";

            echo $row['author_name'] . "<br/>";

        }


    }
}