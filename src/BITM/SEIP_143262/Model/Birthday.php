<?php

namespace App;

use App\Database as DB;

use PDO;

//include("Database.php");

class Birthday extends DB
{


    public $conn;
    public $id = "";

    public $name = "";

    public $birth_date = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function index()
    {

        $conn = $this->DBH;
        $STH = $conn->prepare("select * from birthday");
        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $STH->fetch()) {

            echo "<br/>" . $row['id'] . "<br/>";

            echo $row['name'] . "<br/>";

            echo $row['birth_date'] . "<br/>";

        }

        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_OBJ);
        while ($row = $STH->fetch()) {

            echo "<br/>" . $row->id . "<br/>";

            echo $row->name . "<br/>";

            echo $row->birth_date . "<br/>";


        }


    }
}