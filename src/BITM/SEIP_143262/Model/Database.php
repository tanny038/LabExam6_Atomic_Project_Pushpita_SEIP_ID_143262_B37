<?php
namespace App;
use PDO;

class Database{

public $DBH;
public $host = "localhost";
public $dbName = "atomic_project_B37";
public $user = "root";
public $pass = "";

    public function __construct(){

        try {

            $host=$this->host;
            $dbName=$this->dbName;
            $user=$this->user;
            $pass=$this->pass;

            $this->DBH = new PDO("mysql:host=$host;dbname=$dbName", $user, $pass);
            $this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // set the PDO error mode to exception
            echo "Connected successfully" . "<br/>";

           /*
           $STH = $this->DBH->prepare("select * from book_title");
            $STH->execute();
           */
        }

        catch(PDOException $e){

            echo "I'm sorry, Dave. I'm afraid I can't do that." . "<br/>";

            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);

        }
    }

}
//$obj = new Database();

//print_r(PDO::getAvailableDrivers());



?>